<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Guest;
use App\Rules\ReCaptchaRule;

class GuestController extends Controller
{
    /**
     * Show all RSVP guests.
     *
     * @param  Request $request
     */
    protected function index() {
        $guests = Guest::all();
        return view('admin')->with('guests', $guests);
    }
    /**
     * Create an RSVP guests.
     *
     * @param  Request $request
     */
    protected function create(Request $request) {
        $validatedData = $request->validate([
            'guestFirstName' => 'required|max:30',
            'guestLastName' => 'required|max:30',
            'guestEmail' => 'required|email|max:255|unique:guests,email',
            'guestPhone' => 'regex:/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/',
            'g-recaptcha-response' => ['required', new ReCaptchaRule($request->input('g-recaptcha-response'))]
        ]);
            
        $guest = Guest::create([
            'first_name' => $request->guestFirstName,
            'last_name' => $request->guestLastName,
            'email' => $request->guestEmail,
            'phone' => $request->guestPhone
        ]);

        $guest->save();

        return redirect('/')->with('success', 'RSVP form has submitted successfully!');
    }
}
