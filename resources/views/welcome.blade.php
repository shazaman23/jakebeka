@extends('layouts.app')

@section('content')
<div class="row header-bar justify-content-center align-items-center">
        
    <ul class="menu">
        <li><a href="#story">Our Story</a></li>
        <li><a href="#gallery">Gallery</a></li>
        <li><a href="#reception">Reception</a></li>
        <li><a href="#rsvp">RSVP</a></li>
        <li class="important"><a href="#updates">Updates</a></li>
        <li><a href="#registry">Registry</a></li>
    </ul>

    <a href="javascript:void(0);" class="mobile-menu"><i class="fa fa-bars"></i></a>
            
    <div class="shade"></div>
    <div class="col-md">
        <h1 class="jakeandbeka">Jake and Beka</h1>
        <h2 class="subtitle">are getting married</h2>
    </div>
    <div class="countdown">
        <div class="time-set weeks">
            <div class="title">Weeks</div>
            <div class="digit-set">
                <div class="digit weeks-0">
                    <span class="top">0</span>
                    <span class="top-back">
                        <span>0</span>
                    </span>
                    <span class="bottom">0</span>
                    <span class="bottom-back">
                        <span>0</span>
                    </span>
                </div>
                <div class="digit weeks-1">
                    <span class="top">0</span>
                    <span class="top-back">
                        <span>0</span>
                    </span>
                    <span class="bottom">0</span>
                    <span class="bottom-back">
                        <span>0</span>
                    </span>
                </div>
            </div>
        </div>
        <div class="time-set days">
            <div class="title">Days</div>
            <div class="digit-set">
                <div class="digit days-0">
                    <span class="top">0</span>
                    <span class="top-back">
                        <span>0</span>
                    </span>
                    <span class="bottom">0</span>
                    <span class="bottom-back">
                        <span>0</span>
                    </span>
                </div>
                <div class="digit days-1">
                    <span class="top">0</span>
                    <span class="top-back">
                        <span>0</span>
                    </span>
                    <span class="bottom">0</span>
                    <span class="bottom-back">
                        <span>0</span>
                    </span>
                </div>
            </div>
        </div>
        <div class="time-set hours">
            <div class="title">Hours</div>
            <div class="digit-set">
                <div class="digit hours-0">
                    <span class="top">0</span>
                    <span class="top-back">
                        <span>0</span>
                    </span>
                    <span class="bottom">0</span>
                    <span class="bottom-back">
                        <span>0</span>
                    </span>
                </div>
                <div class="digit hours-1">
                    <span class="top">0</span>
                    <span class="top-back">
                        <span>0</span>
                    </span>
                    <span class="bottom">0</span>
                    <span class="bottom-back">
                        <span>0</span>
                    </span>
                </div>
            </div>
        </div>
        <div class="time-set minutes">
            <div class="title">Minutes</div>
            <div class="digit-set">
                <div class="digit minutes-0">
                    <span class="top">0</span>
                    <span class="top-back">
                        <span>0</span>
                    </span>
                    <span class="bottom">0</span>
                    <span class="bottom-back">
                        <span>0</span>
                    </span>
                </div>
                <div class="digit minutes-1">
                    <span class="top">0</span>
                    <span class="top-back">
                        <span>0</span>
                    </span>
                    <span class="bottom">0</span>
                    <span class="bottom-back">
                        <span>0</span>
                    </span>
                </div>
            </div>
        </div>
        <div class="time-set seconds">
            <div class="title">Seconds</div>
            <div class="digit-set">
                <div class="digit seconds-0">
                    <span class="top">0</span>
                    <span class="top-back">
                        <span>0</span>
                    </span>
                    <span class="bottom">0</span>
                    <span class="bottom-back">
                        <span>0</span>
                    </span>
                </div>
                <div class="digit seconds-1">
                    <span class="top">0</span>
                    <span class="top-back">
                        <span>0</span>
                    </span>
                    <span class="bottom">0</span>
                    <span class="bottom-back">
                        <span>0</span>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="story" class="row align-items-center blue-background">
    <div class="col-12">
        <h1 class= "ourstory">Our Story</h1>
        <hr>
    </div>
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <h3>Beka's Version</h3>
        <p class="versions">
            Our story begins the way all of the best stories do; with ice cream and laughter. Jake and I met in June 2020 on mutual; 
            a dating app targeted towards members of the Church of Jesus Christ of Latter-day Saints. Conversation for the next few days 
            flowed so easily. Jake asked me to meet him at a park in Lehi, Utah for ice cream and a game of disc golf. We talked and laughed, 
            enjoying every moment. As I left the park that night I hoped we would go out again. Jake hadn't asked for my phone number or set 
            up a second date so I was a little nervous that he'd had a different experience than me! But, late that night he messaged me to 
            set up a date for the next day! I fell a sleep that night with a big smile on my face. 

            Since July 2nd, every day has been filled with magic because of Jake. Between phone calls, grocery shopping, dinner, dates, 
            walks, vacations, road trips, and all of the late nights, we haven't spent many waking moments apart! I've loved it all. Jake 
            proposed on September 12th, 2020 at Memory Grove Park in Salt Lake City. That day is one of the happiest that I can remember. 
            
            My life is filled with joy and purpose because of Jake. He's been my supporter and advocate as I've experienced many ups and 
            downs in the past 4 months. I can only imagine how the love I feel for him will increase over a lifetime and beyond! I can't 
            wait to be sealed to him for time and eternity in the Logan, Utah temple. I love you Jake.
        </p> 
    </div>
    <div class="col-md-1"></div>
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <h3>Jake's Version</h3>
        <p class="versions">
            “I am falling hard for Beka.” That’s the first sentence recorded about Beka in my journal. Never has a more true statement been 
            written. 

            I can still remember on our first date walking up to greet her at a park and thinking, “Holy cow, this girl is beautiful.” On 
            that date she made me laugh, and I was able to make her laugh. We spoke about fun topics, and we discussed serious topics. 
            Conversation never got dull, and the whole time I had a smile on my face! I remember thinking, wow, this girl is special, 
            don’t screw this up Jake. From what she has said, she thought I was a little bit interesting so when I was a little crazy 
            and asked her out for the next night, she agreed! 
            
            We spoke or saw each other almost everyday since then. If you know me, you know I am kind of a loud talker, and I remember 
            late nights sitting under a street light so I didn't wake my roommates as I talked with Beka for hours! We had the opportunity 
            to fall in love as we vacationed with each other’s families, and I am sure they thought we were annoying with how infatuated 
            we were with one another! Beka and I are both dreamers, and nothing has made us happier than dreaming of our future together! 
            To end, I want to say that in the same journal entry I mentioned above, I prayed things would work out for us and said I hoped 
            we would get married in December, well here we are, and we are so excited to be build our family!
        </p>
    </div>
    <div class="col-md-1"></div>
</div>
<div id="gallery" class="row align-items-center white-background">
    <div class="col-md"> 
        <h1 class="gallery">Gallery</h1>
        <hr class="blue-line">
        <div class="thumbnail-container">
            <div class="thumbnail thumbnail-border thumbnail-text">
                <img class="engagement-pic" src="/img/opt-almost-kiss-engagement.jpg" alt="Almost kiss engagement picture">
            </div>
            <div class="thumbnail thumbnail-border thumbnail-text">
                <img class="engagement-pic" src="/img/opt-hug-in-the-trees-engagement.jpg" alt="Hug in the trees engagement picture">
            </div>
            <div class="thumbnail thumbnail-border thumbnail-text">
                <img class="engagement-pic" src="/img/opt-path-walking-engagement.jpg" alt="Path walking engagement picture">
            </div>
            <div class="thumbnail thumbnail-border thumbnail-text">
                <img class="engagement-pic" src="/img/opt-wedding-carry-engagement.jpg" alt="Wedding carry engagement picture">
            </div>
        </div>
    </div>
</div>
<div id="reception" class="row align-items-center red-background">
    <div class="col">
        <h1 class="reception">Reception</h1>
        <hr>
        <h3> Please join us for the live stream reception of Jacob Kissell and Rebekah Killpack.</h3>
    </div>
    <div class="row col-12 rec-details align-items-center justify-content-center">
        <div class="col-6">
            <h3 class="rec-details">December 19, 2020</h3>
        </div>
        <div class="col-6">
            <h3 class="rec-details">6-8 PM</h3>
        </div>
    </div>
    <div class="row col-12" style="padding: 0;">
        <div class="col-md-2"></div>
        <div id="twitch-embed" class="col-md-8"></div>
        <div class="col-md-2"></div>
    </div>
</div>
<div id="rsvp" class="row align-items-center white-background">
    <div class="col-md">
        <h1 class="rsvp">RSVP</h1>
        <hr class="red-line">
        <div class="row rsvp-form-section">
            <div class="col-md-2"></div>
            <div class="col-md-8 form-card">
                <form action="/rsvp#rsvp" method="POST">
                    @csrf
                    <div class="form-row">
                        <div class="col-md-6 form-group">
                            <label for="guestFirstName">First Name</label>
                            <input id="guestFirstName" class="form-control @error('guestFirstName') is-invalid @enderror" type="text" name="guestFirstName" placeholder="John" value="{{ old('guestFirstName') }}" required>
                            @error('guestFirstName')
                                <div class="invalid-feedback">
                                    Please enter a valid First Name.
                                </div>
                            @enderror
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="guestLastName">Last Name</label>
                            <input id="guestLastName" class="form-control @error('guestLastName') is-invalid @enderror" type="text" name="guestLastName" placeholder="Doe" value="{{ old('guestLastName') }}" required>
                            @error('guestLastName')
                                <div class="invalid-feedback">
                                    Please enter a valid Last Name.
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6 form-group">
                            <label for="guestEmail">Email</label>
                            <input id="guestEmail" class="form-control @error('guestEmail') is-invalid @enderror" type="text" name="guestEmail" placeholder="johndoe@gmail.com" value="{{ old('guestEmail') }}" required>
                            @error('guestEmail')
                                <div class="invalid-feedback">
                                    Please enter a valid Email.
                                </div>
                            @enderror
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="guestPhone">Phone</label>
                            <input id="guestPhone" class="form-control @error('guestPhone') is-invalid @enderror" type="text" name="guestPhone" placeholder="000-000-0000" value="{{ old('guestPhone') }}" required>
                            @error('guestPhone')
                                <div class="invalid-feedback">
                                    Please enter a valid Phone Number.
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group" style="margin: 0 auto;">
                            <div class="g-recaptcha @error('g-recaptcha-response') is-invalid @enderror" data-sitekey="{{ config('app.recaptcha_api_key') }}"></div>
                            @error('g-recaptcha-response')
                                <div class="invalid-feedback">
                                    ReCaptcha failed. Please check this before submitting.
                                </div>
                            @enderror
                        </div>
                    </div>
                    <button class="btn btn-primary btn-lg btn-block red-theme" type="submit" formmethod="post">Submit RSVP</button>
                </form>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
</div>
<div id="updates" class="row align-items-center green-background">
    <div class="col-md">
        <h1 class="updates">Updates</h1>
        <hr>
        <h3> Come here for updates to the reception or changes due to covid-19. </h3>
        <h3 class="content"> No updates at this time</h3>
    </div>
</div>
<div id="registry" class="row align-items-center justify-content-center white-background registry-form-section">
    <div class="col-12">
        <h1 class="registry">Registry</h1>
        <hr class="green-line">
    </div>
    <div class="col-md-2">
    </div>
    <div class="col-md-4">
        <h3 class="green-words">Jake and Beka are registered on Amazon under "Jake Kissell"</h3>
    </div>
    <div class="col-md-4">
        <div class="registry-cta">
            <a class="green-background" href="https://www.amazon.com/wedding/registry/2CP37XOP46VA7">Get a Gift</a>
        </div>
    </div>
    <div class="col-md-2">
    </div>
</div>
@if (session('success'))
    <div class="alert alert-success rsvp-alert">
        {{ session('success') }}
    </div>
@endif
@endsection

@section('footer')
<!-- Scripts -->
@if (config('app.env') == 'production')
    <script src="{{ asset(mix('js/app.js'), true) }}"></script>
    <script src="{{ asset(mix('js/countdown.js'), true) }}"></script>
@else
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/countdown.js') }}"></script>
@endif
<script src="https://player.twitch.tv/js/embed/v1.js"></script>
<script type="text/javascript">
    new Twitch.Player("twitch-embed", {
        width: "100%",
        height: 450,
        channel: "jakeandbeka2020"
    });

    setInterval(function() {
        $(".rsvp-alert").fadeOut("slow");
    }, 5000);
</script>
@endsection