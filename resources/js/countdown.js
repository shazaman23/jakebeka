require('./bootstrap');

const endtime = new Date("December 19, 2020 19:00:00");

function getTimeLeft(end) {
    const totalTime = Date.parse(end) - Date.parse(new Date());
    const secondsLeft = Math.floor((totalTime / 1000) % 60);
    const minutesLeft = Math.floor((totalTime / (1000 * 60)) % 60);
    const hoursLeft = Math.floor((totalTime / (1000 * 60 * 60)) % 24);
    const daysLeft = Math.floor((totalTime / (1000 * 60 * 60 * 24)) % 7);
    const weeksLeft = Math.floor((totalTime / (1000 * 60 * 60 * 24 * 7)));

    return {
        totalTime,
        secondsLeft,
        minutesLeft,
        hoursLeft,
        daysLeft,
        weeksLeft
    };
}

function updateClock(val, dOne, dTwo) {
    var valOne = val.toString().charAt(0);
    if (val.toString().length > 1) {
        var valTwo = val.toString().charAt(1);
    }
    var dOneOld = dOne.find('.top').text();
    var dTwoOld = dTwo.find('.top').text();

    if (val.toString().length > 1) {
        if (valOne !== dOneOld) {
            // Animate dOne with valOne
            animateDigit(dOne, valOne);
        }
        if (valTwo !== dTwoOld) {
            // Animate dTwo with valTwo
            animateDigit(dTwo, valTwo);
        }
    } else {
        if ('0' !== dOneOld) {
            // Animate dOne with 0
            animateDigit(dOne, '0');
        }
        if (valOne !== dTwoOld) {
            // Animate dTwo with val
            animateDigit(dTwo, val);
        }
    }
}

function animateDigit($el, val) {
    var $top = $el.find('.top');
    var $topBack = $el.find('.top-back');
    var $bottom = $el.find('.bottom');
    var $bottomBack = $el.find('.bottom-back');

    $topBack.find('span').text(val);
    $bottomBack.find('span').text(val);

    TweenMax.to($top, 0.8, {
        rotationX           : '-180deg',
        transformPerspective: 300,
        ease                : Quart.easeOut,
        onComplete          : function() {
            $top.text(val);
            $bottom.text(val);
            TweenMax.set($top, { rotationX: 0 });
        }
    });

    TweenMax.to($topBack, 0.8, { 
        rotationX           : 0,
        transformPerspective: 300,
        ease                : Quart.easeOut, 
        clearProps          : 'all' 
    }); 
}

function ticker() {
    const time = getTimeLeft(endtime);
    if (time.totalTime <= 0) {
        $('.digit .top').each(function(i) {
            $(this).text("0");
        });
        $('.digit .bottom').each(function(i) {
            $(this).text("0");
        });
        return;
    }
    updateClock(time.weeksLeft, $('.weeks-0'), $('.weeks-1'));
    updateClock(time.daysLeft, $('.days-0'), $('.days-1'));
    updateClock(time.hoursLeft, $('.hours-0'), $('.hours-1'));
    updateClock(time.minutesLeft, $('.minutes-0'), $('.minutes-1'));
    updateClock(time.secondsLeft, $('.seconds-0'), $('.seconds-1'));
}

ticker();
var startCountdown = setInterval(ticker, 1000);

$('.mobile-menu').on("click", function() {
    if ($('.menu').hasClass('open')) {
        $('.header-bar').removeClass('show-menu');
        $('.menu').removeClass('open');
    } else {
        $('.header-bar').addClass('show-menu');
        $('.menu').addClass('open');
    }
});