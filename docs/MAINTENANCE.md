# Setup Your Development Environment

1. Install Docker if not already installed. [Download Docker](https://docs.docker.com/engine/install/)

2. Install Docker Compose if not already installed [Download Docker Compose](https://docs.docker.com/compose/install/)

    - **IMPORTANT**
        - If you are using Docker Desktop on Windows or Mac, you get both Docker and Docker Compose at once.
        - If using Docker Desktop on Windows, you will need to enable file sharing to integrate with your development environment. Go to Docker Desktop Settings > Resources > FILE SHARING and add the folder where the killfood project exists to file sharing.

3. Install Nodejs if not already installed [Download Nodejs](https://nodejs.org/en/download/) (most unix distros have some packaged version of this that's easy to install as well)

4. Clone the project code to your computer from [bitbucket](https://bitbucket.org/shazaman23/jakebeka/src/master/) if you have access. Otherwise, unzip the project code you were given and navigate into the project root.

5. Run the following commands from the project root to initialize your local project code (the same level as `package.json`):

    1. Setup NPM node_modules packages (*~15-30s*):

        ```
        npm install
        ```

        (Make sure you have the latest version of Node and NPM before running `npm install` or you could experience problems in step 3)

    2. Compile Sass and VueJS resources:

        ```
        npm run dev
        ```

    3. Setup .env file

        ```
        cp .env.pipelines .env
        ```

        Replace all of the values that start and end with `__`. Some of the variables to be replaced include:

            APP_ENV="testing"
            APP_KEY=""
            APP_URL="http://localhost"
            DB_HOST="db"
            
        The values for `DB_DATABASE`, `DB_USERNAME`, `DB_PASSWORD` are all arbitrary. Whatever you set for them here will need to be mirrored in the future setup step when the docker-compose file is created. If these don't match what you use in the docker-compose config, your local site will not work properly.
        
        Other values are used for 3rd party integration tools. There are common shared values you can use for these, but you must reach out to an existing project maintainer to be granted access. Otherwise, you can generate test resources and provide your own values here. These could include:
        
        - `AWS_KEY`
        - `AWS_SECRET`
        - `AWS_BUCKET`
        - `SLACK_WEBHOOK_URL`
        - `SLACK_NOTIFY_WEBHOOK`

    4. Setup docker-compose.yml file

        1. `cp example.docker-compose.yml docker-compose.yml`

        2. Find the db MySQL settings section and make sure to set the variables `MYSQL_DATABASE`, `MYSQL_USER`, and `MYSQL_PASSWORD` to match the values you used for the `DB_*` variables in the .env file from the last step.

6. Deploy docker containers (*~10mins*)

    ```
    docker-compose up -d
    ```

    (This command can take a while the first time it runs)

7. Install composer packages (*~15mins*)

    ```
    docker-compose exec app composer install
    ```
    (Note on the length: This step takes a really long time, but only really needs to be run once from scratch on a new dev system. All future composer tweaks only will deal with a handful of package installs, but during initial set up it installs 120+ packages in one go. Additionally, it is slowed down by communicating across the docker network. If you already have `composer` installed on your machine, you can run the `composer install` locally to speed it up a bit. However, based off of my experiences with setting up `composer` on a new machine, unless `composer` is already on your machine this docker approach is probably much faster)

8. Generate an app key for the development environment

    ```
    docker-compose exec app php artisan key:generate
    ```

9. Initialize database

    ```
    docker-compose exec app php artisan migrate --seed
    ```

10. Clear caches and create config

    ```
    docker-compose exec app php artisan cache:clear
    docker-compose exec app php artisan route:clear
    docker-compose exec app php artisan view:clear
    docker-compose exec app php artisan config:cache
    ```

---

# Using Your Testing Environment

Now that your code base and docker cluster has been deployed, you're ready to start coding! The project code should now be tied to the code running in the docker containers so that changes you make take effect in real-time.

To interact with the local website that is running visit http://localhost
- Make sure your docker cluster is running by running `docker-compose ps`. If the containers aren't running you won't be able to get to the local site. If they're down, just bring them back up with `docker-compose up -d`

The database initialization script you ran set up a test user for you to work with.

**Username:** test@test.com
**Password:** secret

You are also free to set up your own test user by registering it on your local site like a new user would on the live site (see User Guide).

You can work directly with the testing database using phpMyAdmin. To access phpMyAdmin on your local cluster, visit http://localhost:8080

The username and password you use to login here will match the database credentials that you set in your `.env` and `docker-compose.yml` files.

---


# Maintenance Tips
- When making changes to the application using built in Laravel artisan commands, make sure the commands are being run from inside the docker cluster. To make this happen run the following command:

    ```
    docker-compose exec app <php-artisan-command>
    ```

- You can also tunnel into the container that runs the app to run a number of artisan commands in a row as follows:

    ```
    docker-compose exec -it app bash
    ```

- Beware, phpunit is not in the path in the docker image. Running phpunit tests can be done with the following command:

    ```
    docker-compose exec app ./vendor/bin/phpunit --colors="always"
    ```

- If you have to add to the application image, make sure to rebuild the image when you start local development to test the image:

    ```
    docker-compose up -d --build
    ```

- Laravel Mix is a tool used to minify and compile CSS and JS resources. When updating js or sass assets, you need to run the following while testing:

    ```
    npm run dev
    ```

- (After upgrading laravel versions I hope this changes but for now...) The build process doesn't properly compile resources when `npm run ...` is executed. For now, this means that when updates are made, you need to run the following to make sure that resources compile properly and cache busting occurs:

    ```
    npm run prod
    ```
    
---

# Cleanup

When you're done with the environment, you can clean up with the following:

```
docker-compose down
```

Docker tends to build up a lot of space. You can do some research around cleaning up docker on your machine with automated scripts, but the docker recommendation is to at least do the following to clean up unused docker resources:

1. Make sure that your current docker deployment is running (this is to make sure that you don't delete your current iteration of the app/database/etc)

```
docker-compose ps
```
(your resources should show as `Up`. If they're not there, run `docker-compose up -d`)

2. Run the docker cleanup command to delete unused images, volumes, networks, etc from your system.

```
docker system prune -a
```
(This command can take some time)