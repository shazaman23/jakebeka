@extends('layouts.app')

@section('content')
<div id="rsvp-results" class="row red-background">
    <div class="col-12">
        <h2 class="subtitle">RSVP Results</h2>
    </div>
    <div class="col-2"></div>
    <div class="col-8">
        <table class="table table-striped">
            <thead>
                <th scope="col">First Name</th>
                <th scope="col">Last Name</th>
                <th scope="col">Email</th>
            </thead>
            <tbody>
                @foreach ($guests as $guest)
                    <tr>
                        <td>{{ $guest->first_name }}</td>
                        <td>{{ $guest->last_name }}</td>
                        <td>{{ $guest->email }}</td>
                    </tr>
                @endforeach
                <tr>
                    <td><strong>Total:</strong> {{ count($guests) }} Guests</td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-2"></div>
</div>
@endsection

@section('footer')
<!-- Scripts -->
@if (config('app.env') == 'production')
    <script src="{{ asset(mix('js/app.js'), true) }}"></script>
@else
    <script src="{{ asset('js/app.js') }}"></script>
@endif
@endsection