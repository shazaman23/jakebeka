<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use ReCaptcha\ReCaptcha;

class ReCaptchaRule implements Rule
{
    private $err_msg = "";
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (empty($value)) {
            $this->err_msg = 'ReCaptcha field is required';
            return false;
        }
        $recaptcha = new ReCaptcha(config('app.recaptcha_api_secret'));
        $res = $recaptcha->setExpectedHostname(config('app.hostname'))
                         ->verify($value, $_SERVER['REMOTE_ADDR']);
        
        if (!$res->isSuccess()) {
            $this->err_msg = 'ReCaptcha field is required';
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->err_msg;
    }
}
