# Setting up for the first time
1. Copy `example.docker-compose.yml` to `docker-compose.yml`
2. Add values (arbitrary) for the database variables in the `app` and `db` sections of the `docker-compose.yml` (the values must match between the two sections)
3. Run `dcup -d`

# Starting the site for testing
1. Run `dcps`
  - If the response shows deployments with a state of "Up" you're ready to go! Skip to step 3.
2. Run `dcup -d` and wait till the services that are starting all say "done".
3. Go to [the local testing site](http://jakeandbeka.local)

# Ideas for Wedding Site
### Free Theme
[Vacation Booking Site](https://themeisle.com/demo/?theme=Vacation%20Rental#)

### Paid Theme (for ideas)
[Wedding Site](https://themeforest.net/item/monument-responsive-wordpress-wedding-theme/7998985)

### Cool Image Scrolling (Parallax Images)
[Parallax Image Article](https://medium.com/@bretcameron/parallax-images-sticky-footers-and-more-8-useful-css-tricks-eef12418f676)

# Current website TODO list
☑ Add 2 more sections with a green theme for additional content

☐ Add a form for RSVP (should ask for a name and email address at least)

☑ Add a countdown clock to the title section

☑ Add content to the "Our Story" section

☐ Add content to the registry section

☐ Add a section with info about time and place of the reception

☑ Add a section for any "Last Minute Updates"

☑ Add a navbar of some sort

☐ Add engagement pictures to the Gallery section