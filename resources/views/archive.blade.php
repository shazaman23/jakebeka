@extends('layouts.app')

@section('content')
<div id="reception" class="row align-items-center red-background">
    <div class="col">
        <h1 class="reception">Reception</h1>
        <hr>
        <h3> Please join us to celebrate the marriage of Jacob Kissell and Rebekah Killpack.</h3>
    </div>
    <div class="row col-12 rec-details align-items-center justify-content-center">
        <div class="col-3">
            <h3 class="rec-details">December 19, <br>2020</h3>
        </div>
        <div class="col-6">
            <h3 class="rec-details">Logan Institute of Religion</h3>
            <h3 class="content">600 darwin Ave, Logan, UT 84321</h3>
        </div>
        <div class="col-3">
            <h3 class="rec-details">6-8 PM</h3>
        </div>
    </div>
    <div class="row col-12" style="padding: 0;">
        <div class="col-1"></div>
        <iframe
            class="col-10"
            width="100%"
            height="450"
            frameborder="0" style="border:0"
            src="https://www.google.com/maps/embed/v1/place?key={{ config('app.maps_api_key') }}
                &q=600+Darwin+Ave,Logan+UT" allowfullscreen>
            </iframe>
        <div class="col-1"></div>
    </div>
    <div class="col-12 rec-details">
        <h3>Parking</h3>
    </div>
</div>
@endsection